/**
 * @module kadence
 * @license AGPL-3.0
 * @author Gordon Hall https://github.com/bookchin
 */

'use strict';

/**
 * Returns a new {@link KademliaNode}
 */
module.exports = function(options) {
  return new module.exports.KademliaNode(options);
};

/** {@link KademliaNode} */
module.exports.KademliaNode = require('./lib/node-kademlia');

/** {@link KademliaRules} */
module.exports.KademliaRules = require('./lib/rules-kademlia');

/** {@link AbstractNode} */
module.exports.AbstractNode = require('./lib/node-abstract');

/** {@link ErrorRules} */
module.exports.ErrorRules = require('./lib/rules-errors');

/** {@link Bucket} */
module.exports.Bucket = require('./lib/bucket');

/** {@link Control} */
module.exports.Control = require('./lib/control');

/** {@link Messenger} */
module.exports.Messenger = require('./lib/messenger');

/** {@link RoutingTable} */
module.exports.RoutingTable = require('./lib/routing-table');

/** {@link UDPTransport} */
module.exports.UDPTransport = require('./lib/transport/udp');

/** {@link HTTPTransport} */
module.exports.HTTPTransport = require('./lib/transport/http');

/** {@link HTTPSTransport} */
module.exports.HTTPSTransport = require('./lib/transport/https');

/** {@link module:kadence/hashcash} */
module.exports.hashcash = require('./lib/plugins/hashcash');

/** {@link module:kadence/hibernate} */
module.exports.hibernate = require('./lib/plugins/hibernate');

/** {@link module:kadence/onion} */
module.exports.onion = require('./lib/plugins/onion');

/** {@link module:kadence/quasar} */
module.exports.quasar = require('./lib/plugins/quasar');

/** {@link module:kadence/spartacus} */
module.exports.spartacus = require('./lib/plugins/spartacus');

/** {@link module:kadence/traverse} */
module.exports.traverse = require('./lib/plugins/traverse');

/** {@link module:kadence/eclipse} */
module.exports.eclipse = require('./lib/plugins/eclipse');

/** {@link module:kadence/permission} */
module.exports.permission = require('./lib/plugins/permission');

/** {@link module:kadence/rolodex} */
module.exports.rolodex = require('./lib/plugins/rolodex');

/** {@link module:kadence/contentaddress} */
module.exports.contentaddress = require('./lib/plugins/contentaddress');

/** {@link module:kadence/trust} */
module.exports.trust = require('./lib/plugins/trust');

/** {@link module:kadence/logger} */
module.exports.logger = require('./lib/plugins/logger');

/** {@link module:kadence/churnfilter} */
module.exports.churnfilter = require('./lib/plugins/churnfilter');

/** {@link module:kadence/constants} */
module.exports.constants = require('./lib/constants');

/** {@link module:kadence/version} */
module.exports.version = require('./lib/version');

/** {@link module:kadence/utils} */
module.exports.utils = require('./lib/utils');


